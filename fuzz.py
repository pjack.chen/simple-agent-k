from pythonfuzz.main import PythonFuzz


@PythonFuzz
def fuzz(buf):
    print(buf)


if __name__ == '__main__':
    fuzz()
